/* eslint-disable prefer-template */

const path = require('path');

// ///////////////////////////////////////////////////////////////
// //////////////////   PLUGINS   ////////////////////////////////
// ///////////////////////////////////////////////////////////////

const commonPlugins = [
  [
    require.resolve('babel-plugin-module-resolver'),
    {
      root: [path.resolve('./')],
      alias: {
        screens: './src/screens',
        routes: './src/routes',
        config: './src/config',
        store: './src/store',
        hooks: './src/hooks',
        styles: './src/styles',
        '@types': './src/@types',
        helpers: './src/helpers',
        components: './src/components',
        services: './src/services',
      },
    },
  ],
];

module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: commonPlugins,
};
