/**
 * @format
 */

import React from 'react';
import { StatusBar } from 'react-native';
import { Provider } from 'react-redux';
import { store } from './src/store';

import { DefaultTheme, ThemeProvider } from 'styled-components';

import getTheme from 'helpers/getTheme';

import Routes from './src/routes';

import('./src/config/reactotron').then(() =>
  console.log('Reactotron Configured'),
);

const App = () => {
  const theme = getTheme('light');

  return (
    <ThemeProvider theme={theme as DefaultTheme}>
      <Provider store={store}>
        <StatusBar barStyle="dark-content" />
        <Routes />
      </Provider>
    </ThemeProvider>
  );
};

export default App;
