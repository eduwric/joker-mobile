import Theme, { ThemeOptions } from 'styles/theme';

export const getTheme = (opt: ThemeOptions) =>
  Theme.getInstance().getTheme(opt);

export default getTheme;
