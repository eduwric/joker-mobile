import AsyncStorage from '@react-native-community/async-storage';
import Reactotron from 'reactotron-react-native';

type Msg = (message?: any, ...optionalParams: any[]) => void;

if (__DEV__) {
  const tron = Reactotron.setAsyncStorageHandler!(AsyncStorage)
    .configure({
      name: 'JokesApp',
    })
    .useReactNative()
    .connect();

  // .use(sagaPlugin({}))
  tron.clear!();
}

// Reactotron.log!('hey there');

// console.tron = tron;
// console.log = Reactotron.log as Msg;
// console.warn = Reactotron.warn as Msg;
// console.display = Reactotron.display;
// console.error = Reactotron.error as Msg;
