import { combineReducers } from 'redux';

import App from './app/app.reducer';

// Add your reducers here
const appReducer = combineReducers({
  App,
});

const rootReducer = (state: any, action: any) => {
  return appReducer(state, action);
};

export default rootReducer;
