import { combineReducers } from 'redux';
import init from './init/reducer';
import jokes from './jokes/reducer';

const AppReducer = combineReducers({
  init,
  jokes,
});

export default AppReducer;
