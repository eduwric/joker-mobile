import AppInitAction from './actions';
import { AppInitializationState, AppInitActionTypes } from './types';

// Setup the reducer initial state
const initialState: AppInitializationState = {
  loading: false,
  currentDay: null,
};

const reducer = (
  state: AppInitializationState = initialState,
  action: AppInitActionTypes,
): AppInitializationState => {
  switch (action.type) {
    case AppInitAction.START:
      return {
        ...state,
        loading: true,
      };
    case AppInitAction.SET_DAY:
      console.log('payload', action.payload);
      return {
        ...state,
        currentDay: action.payload,
        loading: false,
      };

    default:
      return state;
  }
};

export default reducer;
