import AppInitAction from './actions';

// Declare Initial state type
export interface AppInitializationState {
  readonly loading: boolean;
  readonly currentDay: any;
}

// Declare Action types based on action functions
export type AppInitActionTypes = ReturnType<
  typeof AppInitAction.setDay | typeof AppInitAction.start
>;
