import { takeLatest, put } from 'redux-saga/effects';

import AppInitAction from './actions';

function* handleInit() {
  console.log('innniiiitttt ');

  const day = new Date().getDate();

  yield put(AppInitAction.setDay(day));
}

// Listen to the actions and triggers the functions
export function* onAppInitializationStart() {
  yield takeLatest(AppInitAction.START, handleInit);
}
