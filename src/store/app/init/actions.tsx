// Declare an action and action types as static methods of a class

export default class AppInitAction {
  static START = 'AppInitAction.IS_IDENTIFIER_ELIGIBLE';
  static SET_DAY = 'AppInitAction.SET_DAY';

  static start = () => ({ type: AppInitAction.START, payload: null });
  static setDay = (day: number) => ({
    type: AppInitAction.SET_DAY,
    payload: day,
  });
}
