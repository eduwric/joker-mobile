import { all, call } from 'redux-saga/effects';
import { onAppInitializationStart } from './init/sagas';
import { onJokesActions } from './jokes/sagas';

export function* AppSagas() {
  yield all([call(onAppInitializationStart), call(onJokesActions)]);
}
