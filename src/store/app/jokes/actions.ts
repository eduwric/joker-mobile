// Declare an action and action types as static methods of a class

import { FindJokesResponse } from '../../../services/jokesApi';
import { JokesActionTypesEnum } from './types';

class JokesActions {
  static findJokesRequest = () => ({
    type: JokesActionTypesEnum.FIND_JOKES_REQUEST,
  });
  static findJokesSuccess = (payload: FindJokesResponse) => ({
    type: JokesActionTypesEnum.FIND_JOKES_SUCCESS,
    payload,
  });
  static findJokesFailure = (errors: any) => ({
    type: JokesActionTypesEnum.FIND_JOKES_FAILURE,
    payload: errors,
  });
}

export default JokesActions;
