import { JokesActionTypesEnum } from './types';
import api from 'services/jokesApi';
import JokesActions from './actions';
import { put, call, takeLatest } from 'redux-saga/effects';

function* handleFindJokes() {
  try {
    const response = yield call(api.getInstance().findJokes, {});
    yield put(JokesActions.findJokesSuccess(response.data));
  } catch (err) {
    yield put(JokesActions.findJokesFailure(err));
  }
}

// Listen to the actions and triggers the functions
export function* onJokesActions() {
  yield takeLatest(JokesActionTypesEnum.FIND_JOKES_REQUEST, handleFindJokes);
}
