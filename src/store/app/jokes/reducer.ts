import { JokesActionTypesEnum, JokesState, JokesActionTypes } from './types';

// Setup the reducer initial state
const initialState: JokesState = {
  loading: false,
  error: false,
};

const reducer = (
  state: JokesState = initialState,
  action: JokesActionTypes,
): JokesState => {
  switch (action.type) {
    case JokesActionTypesEnum.FIND_JOKES_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case JokesActionTypesEnum.FIND_JOKES_SUCCESS:
      console.log('payload', action.payload);
      return {
        ...state,
        data: action.payload,
        loading: false,
      };
    case JokesActionTypesEnum.FIND_JOKES_FAILURE:
      return {
        ...initialState,
        error: action.payload,
      };
    default:
      return state;
  }
};

export default reducer;
