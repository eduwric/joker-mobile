import JokesActions from './actions';
import { FindJokesResponse } from '../../../services/jokesApi';

// enum
export enum JokesActionTypesEnum {
  FIND_JOKES_REQUEST = '@jokesActions.FIND_JOKES_REQUEST',
  FIND_JOKES_SUCCESS = '@jokesActions.FIND_JOKES_SUCCESS',
  FIND_JOKES_FAILURE = '@jokesActions.FIND_JOKES_FAILURE',
}

// Declare Initial state type
export interface JokesState {
  readonly loading: boolean;
  readonly error: any;
  readonly data?: FindJokesResponse;
}

const { findJokesFailure, findJokesRequest, findJokesSuccess } = JokesActions;

export type JokesActionTypes = ReturnType<
  typeof findJokesRequest & typeof findJokesSuccess & typeof findJokesFailure
>;
