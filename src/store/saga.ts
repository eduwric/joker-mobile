import { all, call } from 'redux-saga/effects';
import { AppSagas } from './app/app.sagas';

export default function* rootSaga() {
  yield all([
    call(AppSagas),
    //     call(AuthSagas),
    //     call(EligibilitySagas),
    //     call(SessionSagas),
    //     call(IntelligenceSagas),
    //     call(EmotionSagas),
    //     call(UserSagas),
    //     call(BenefitSagas),
    //     spawn(watchAlertChannel),
  ]);
}
