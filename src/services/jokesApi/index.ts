import axios, { AxiosResponse } from 'axios';

import {
  CreateJokesDto,
  FindJokesQueryParams,
  FindJokesResponse,
  Joke,
} from './jokesApi.interface';
// exports a class with the instance of axios,
// The singleton pattern ensures a single instance of axios.

export class JokesApiService {
  private static instance: JokesApiService;

  constructor() {
    if (JokesApiService.instance) {
      throw new Error(
        'Error: Instantiation failed: Use JokesApiService.getInstance() instead of new.',
      );
    }
    JokesApiService.instance = this;
  }

  public static getInstance(): JokesApiService {
    if (!JokesApiService.instance) {
      JokesApiService.instance = new JokesApiService();
    }

    return JokesApiService.instance;
  }

  public api = axios.create({
    baseURL: 'https://api.jokes.wronscki.com/v1',
  });

  public findJokes = async (
    params: FindJokesQueryParams | undefined,
  ): Promise<AxiosResponse<FindJokesResponse>> => {
    if (!params) {
      return this.api.get('/jokes');
    }

    const queryCount = Object.keys(params).length;

    const queryString = Object.entries(params).reduce((acc, curr, idx) => {
      console.log('acc', acc, idx);
      if (idx === 0) {
        return `${curr[0]}=${curr[1]}`;
      }

      return `${acc}&${curr[0]}=${curr[1]}`;
    }, '');

    if (queryCount) {
      console.log('query', queryString);
      return this.api.get(`/jokes?${queryString}`);
    }

    return this.api.get('/jokes');
  };

  public createJokes = (body: CreateJokesDto): Promise<AxiosResponse<Joke>> => {
    return this.api.post('/jokes', body);
  };
}

export * from './jokesApi.interface';

export default JokesApiService;
