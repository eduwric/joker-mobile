export interface Joke {
  id: number;
  title: string;
  description: string;
  answer: string;
  status: 'APPROVED' | 'PENDING' | 'DISABLED';
  created_at: string;
  updated_at: string;
}

export interface FindJokesResponse {
  items: Joke[];
  meta: {
    totalItems: number;
    itemCount: number;
    itemsPerPage: number;
    totalPages: number;
    currentPage: number;
  };
}

export interface FindJokesQueryParams {
  limit?: number;
  page?: number;
  search?: string;
  status?: string;
}

export interface CreateJokesDto {
  title: string;
  description: string;
  answer: string;
}
