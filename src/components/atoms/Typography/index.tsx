import { normalize } from 'helpers/normalize';
import styled from 'styled-components/native';

const FontRegular = styled.Text`
  /* Default font styles here */
`;

const FontBold = styled.Text`
  /* Default font styles here */
`;

export const H1Heading = styled(FontRegular)`
  font-size: ${normalize(40)}px;
`;

export const Body = styled(FontRegular)`
  font-size: ${normalize(16)}px;
`;

export const ButtonText = styled(FontBold)`
  text-transform: uppercase;
  letter-spacing: ${normalize(4)}px;
  font-size: ${normalize(20)}px;
  font-weight: bold;
  color: ${({ theme }) => theme.colors.button.textActive};
`;
