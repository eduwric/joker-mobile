import { normalize } from 'helpers/normalize';
import styled from 'styled-components/native';

export const SubmitButton = styled.TouchableOpacity`
  flex-direction: row;
  border-radius: ${normalize(8)}px;
  height: ${normalize(40)}px;
  background-color: ${({ theme }) => theme.colors.button.bgActive};
  justify-content: center;
  align-items: center;
`;

export default SubmitButton;
