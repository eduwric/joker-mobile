import styled from 'styled-components/native';
import { normalize } from 'helpers/normalize';

interface Props {
  size?: number;
}

export const Spacer = styled.View<Props>`
  margin: ${({ size }) => (size ? normalize(size) : normalize(8))}px;
`;
