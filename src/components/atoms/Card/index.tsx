import styled from 'styled-components/native';
import { normalize } from 'helpers/normalize';

export const Card = styled.View`
  width: 100%;
  padding-horizontal: ${normalize(16)}px;
  padding-vertical: ${normalize(32)}px;
  padding: ${normalize(32)}px;
  elevation: 2;
`;
