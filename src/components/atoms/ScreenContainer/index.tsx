import styled from 'styled-components/native';

export const ScreenContainer = styled.View`
  align-items: center;
  flex: 1;
  width: 100%;
  background-color: ${({ theme }) => theme.colors.background};
`;

export default ScreenContainer;
