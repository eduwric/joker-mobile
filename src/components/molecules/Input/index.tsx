import { Spacer } from 'components/atoms/Spacer';
import { normalize } from 'helpers/normalize';
import React from 'react';
import { StyledInput, InputContainer, Label } from './styles';

interface InputProps {
  label?: string;
  onFocus?: () => void;
  onBlur?: () => void;
  disabled?: boolean;
  onChangeText?: (value: any) => void;
  value?: string;
  error?: any;
}

export const Input = ({ label, ...inputProps }: InputProps): JSX.Element => {
  return (
    <InputContainer>
      <Label>{label}</Label>
      <Spacer size={normalize(8)} />
      <StyledInput {...inputProps} />
    </InputContainer>
  );
};
