import styled from 'styled-components/native';
import { normalize } from 'helpers/normalize';
import { Body } from '../../atoms/Typography';

export const InputContainer = styled.View`
  width: 100%;
`;

export const StyledInput = styled.TextInput`
  flex-direction: row;
  border-width: 2px;
  border-color: ${({ theme }) => theme.colors.input.colorActive};
  height: ${normalize(40)}px;
  border-radius: ${normalize(8)}px;
  padding-horizontal: ${normalize(12)}px;
  font-size: ${normalize(16)}px;
`;

export const Label = styled(Body)`
  color: ${({ theme }) => theme.colors.input.colorActive};
`;
