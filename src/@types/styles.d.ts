import 'styled-components';
import { ThemeInterface } from 'styles/theme';

// Declare theme based on ThemeInterface
declare module 'styled-components' {
  export interface DefaultTheme extends ThemeInterface {}
}
