export interface ThemeInterface {
  colors: {
    textPrimary: string;
    background: string;
    input: {
      colorActive: string;
      colorError: string;
    };
    button: {
      bgActive: string;
      bgDisabled: string;
      textActive: string;
      textDisabled: string;
    };
  };
}

export type ThemeOptions = 'light' | 'dark';

export default class Theme {
  private static instance: Theme;

  private lightTheme: ThemeInterface = {
    colors: {
      textPrimary: 'black',
      background: 'white',
      input: {
        colorActive: '#58508d',
        colorError: '#ff6361',
      },
      button: {
        bgActive: '#06c1ff',
        bgDisabled: 'black',
        textActive: 'white',
        textDisabled: 'white',
      },
    },
  };

  private darkTheme: ThemeInterface = {
    colors: {
      textPrimary: 'white',
      background: 'black',
      input: {
        colorActive: '#58508d',
        colorError: '#ff6361',
      },
      button: {
        bgActive: 'green',
        bgDisabled: 'black',
        textActive: 'white',
        textDisabled: 'white',
      },
    },
  };

  constructor() {
    if (Theme.instance) {
      throw new Error(
        'Error: Instantiation failed: Use Theme.getInstance() instead of new.',
      );
    }
    Theme.instance = this;
  }

  public static getInstance(): Theme {
    if (!Theme.instance) {
      Theme.instance = new Theme();
    }

    return Theme.instance;
  }

  public getTheme(key: ThemeOptions): ThemeInterface {
    switch (key) {
      case 'light':
        return this.lightTheme;
      case 'dark':
        return this.darkTheme;
      default:
        return this.lightTheme;
    }
  }
}
