/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';

import { View, Text, FlatList } from 'react-native';
import { useDispatch } from 'react-redux';

import { useTypedSelector } from 'hooks';

import JokesActions from 'store/app/jokes/actions';

export function FeedScreen() {
  const dispatch = useDispatch();

  const { data } = useTypedSelector((state) => state.App.jokes);

  useEffect(() => {
    dispatch(JokesActions.findJokesRequest());
  }, [dispatch]);

  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <View
        style={{
          width: '100%',
          height: 50,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text>Painel de piadas</Text>
      </View>
      {data && (
        <FlatList
          data={data.items}
          style={{
            height: 150,
            width: '100%',
          }}
          renderItem={({ item }) => {
            if (item.status === 'PENDING') {
              return <></>;
            }
            return (
              <View key={item.id} style={{ margin: 5 }}>
                <Text>{item.description}</Text>
                <Text>{item.answer}</Text>
              </View>
            );
          }}
        />
      )}
    </View>
  );
}
