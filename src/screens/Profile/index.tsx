/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';

import { useForm, Controller } from 'react-hook-form';
import { Input } from 'components/molecules/Input';
import { ScreenContainer } from 'components/atoms/ScreenContainer';
import { Card } from 'components/atoms/Card';
import { SubmitButton } from 'components/atoms/SubmitButton';
import { ButtonText } from 'components/atoms/Typography';
import { Spacer } from 'components/atoms/Spacer';
import { ScrollView } from 'react-native-gesture-handler';
import { JokesApiService } from 'services/jokesApi';
interface FormData {
  title: string;
  description: string;
  answer: string;
}

export function ProfileScreen() {
  const { control, handleSubmit, errors, reset } = useForm<FormData>();

  const [submittedData, setSubmittedData] = useState<any>({});

  useEffect(() => {
    reset({
      answer: '',
      description: '',
      title: '',
    });
  }, [submittedData, reset]);

  // I can't reset the form... investigate
  const onSubmit = handleSubmit(async (fields) => {
    setSubmittedData(fields);
    JokesApiService.getInstance().createJokes({ ...fields });
  });

  return (
    <ScreenContainer
      style={{
        paddingHorizontal: 16,
        paddingVertical: 32,
      }}>
      <ScrollView>
        <ScreenContainer
          style={{
            flexDirection: 'row',
            flex: 1,
          }}>
          <Card>
            <Controller
              control={control}
              name="title"
              rules={{ required: true }}
              defaultValue=""
              render={({ onChange, onBlur, value }) => (
                <Input
                  label="Name"
                  onBlur={onBlur}
                  value={value}
                  onChangeText={(val) => onChange(val)}
                  error={errors.title}
                />
              )}
            />
            <Spacer />
            <Controller
              control={control}
              name="description"
              rules={{ required: true }}
              defaultValue=""
              render={({ onChange, onBlur, value }) => (
                <Input
                  label="Description"
                  onBlur={onBlur}
                  value={value}
                  onChangeText={(val) => onChange(val)}
                  error={errors.title}
                />
              )}
            />
            <Spacer />
            <Controller
              control={control}
              name="answer"
              rules={{ required: true }}
              defaultValue=""
              render={({ onChange, onBlur, value }) => (
                <Input
                  label="Answer"
                  onBlur={onBlur}
                  value={value}
                  onChangeText={(val) => onChange(val)}
                  error={errors.title}
                />
              )}
            />
            <Spacer size={16} />
            <SubmitButton onPress={() => onSubmit()}>
              <ButtonText>Submit</ButtonText>
            </SubmitButton>
          </Card>
        </ScreenContainer>
      </ScrollView>
    </ScreenContainer>
  );
}
