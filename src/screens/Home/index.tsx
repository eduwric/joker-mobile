/* eslint-disable react-native/no-inline-styles */
import React, { useEffect } from 'react';
import { useNavigation, useRoute } from '@react-navigation/native';
import { View, Text, TouchableOpacity, ScrollView } from 'react-native';
import { NavigationProp, RouteProps } from 'routes';

export function HomeScreen() {
  const route = useRoute<RouteProps<'Home'>>();
  const navigation = useNavigation<NavigationProp<'Home'>>();

  useEffect(() => {
    console.log('home screen');
  }, []);

  return (
    <ScrollView contentContainerStyle={{ flex: 1 }}>
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>{route.name} Screen</Text>
        <TouchableOpacity
          style={{ backgroundColor: 'green' }}
          onPress={() => navigation.navigate('Feed')}>
          <Text style={{ color: 'black', fontSize: 20 }}>Go to Feed</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{ backgroundColor: 'purple' }}
          onPress={() => navigation.navigate('Profile')}>
          <Text style={{ color: 'black', fontSize: 20 }}>Go to Profile</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
}
